﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Ports;
using System.Diagnostics;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            string dStart = DateTime.Now.ToString("yyyy.MM.dd HH-mm-ss");
            string fileName = "cfg\\config.cfg";
            string cmdFileName = "cfg\\cmd.cfg";
            string cfgCOM = "", cfgPhones = "", outCmd = "";
            string dt = "", data = "";
            string text = "";
            string sender = "";
            int c = 0, messg=0;
            string[] cfgFile;
            Dictionary<string, string> cmd = new Dictionary<string, string>();
            if (!Directory.Exists("log"))
            {
                Directory.CreateDirectory("log");
            }
            if (!Directory.Exists("cfg"))
            {
                Directory.CreateDirectory("cfg");
            }
            // Create CFG
            // для отладки:
            // File.Delete(fileName);
            if (!System.IO.File.Exists(fileName))
            {
                // рабочее:
                File.AppendAllText(fileName, "COM=\r\n");
                File.AppendAllText(fileName, "PHONES=\r\n");
            }
            // end
            // Read CFG
            cfgFile = System.IO.File.ReadAllLines(fileName);
            foreach (string line in cfgFile)
            {
                string[] cfgFileLine = line.Split(new Char[] { '=' });
                cfgFileLine[1] = cfgFileLine[1].Trim();
                switch (cfgFileLine[0])
                {
                    case "COM":
                        cfgCOM = cfgFileLine[1];
                        break;
                    case "PHONES":
                        cfgPhones = cfgFileLine[1];
                        break;
                }
            }
            // end
            // Check CFG
            if (cfgCOM == "") Console.WriteLine("Err: COM не задан");
            //if (cfgPhones == "") Console.WriteLine("Err: PHONES не задан");
            //if (cfgCOM == "" || cfgPhones == "") Environment.Exit(0);
            if (cfgCOM == "") Environment.Exit(0);
            Console.WriteLine("Используем COM порт \t-> COM" + cfgCOM);
            if (cfgPhones.Length == 0) Console.WriteLine("Принимаем комманды от \t-> any");
            else Console.WriteLine("Принимаем комманды от \t-> " + cfgPhones);
            // end
            // Create CMD
            // для отладки:
            //File.Delete(cmdFileName);
            if (!System.IO.File.Exists(cmdFileName))
            {
                // для отладки:
                //File.AppendAllText(cmdFileName, "Ромашка,run.bat\r\n");
                //File.AppendAllText(cmdFileName, "Валим!,shutdown /f /r /t 0\r\n");
                // рабочее:
                File.AppendAllText(cmdFileName, "");
            }
            // end
            // Read CMD
            cfgFile = System.IO.File.ReadAllLines(cmdFileName);
            Console.WriteLine("Команды:");
            foreach (string line in cfgFile)
            {
                string[] cfgFileLine = line.Split(new Char[] { ',' });
                cfgFileLine[0] = cfgFileLine[0].Trim();
                cfgFileLine[1] = cfgFileLine[1].Trim();
                Console.Write("\t");
                Console.Write(cfgFileLine[0]);
                Console.Write("\t->\t");
                Console.WriteLine(cfgFileLine[1]);
                if (cfgFileLine[0] != "" && cfgFileLine[1] != "")
                {
                    cmd[cfgFileLine[0]] = cfgFileLine[1];
                }
            }
            // end
            SerialPort port = new SerialPort("COM" + cfgCOM, 9600, Parity.None, 8, StopBits.One);
            port.Handshake = Handshake.RequestToSend;
            while (true)
            {
                try
                {
                    System.Threading.Thread.Sleep(1000);
                    if (!port.IsOpen)
                    {
                        Console.WriteLine("Порт закрыт, открываем");
                        toLog("[ERROR] Порт закрыт, открываем", dStart);
                        port.Open();
                        // устанавливаем формат, место хранения, отключаем уведомления о доставке, удаляем все сообщения
                        port.WriteLine("AT+CMGF=1;+CPMS=\"SM\";+CSMS=0;+CMGD=1,4\r\n");
                        port.ReadLine();
                    }
                    port.WriteLine("AT+CMGR=0;+CMGF=1;+CMGD=1,4\r\n");
                    dt = port.ReadExisting();
                    dt = dt.Trim();
                    dt = dt.Replace("\n", ",");
                    dt = dt.Replace("\"", "");
                    string[] lines = dt.Split(new Char[] { ',' });
                    for (c = 0; c < lines.Length; c++)
                    {
                        data = lines[c].Trim();
                        if (data == "" || data == "OK")
                        {
                            if (data == "" && lines.Length == 1)
                            {
                                port.WriteLine("ATZ" + Environment.NewLine);
                                dt = port.ReadLine();
                                dt = data.Trim();
                                if (dt == "")
                                {
                                    Console.WriteLine("Err: Не получил ответ. Закрываем порт.");
                                    toLog("[ERROR] Не получил ответ, или не правильный ответ. Закрываем порт. Вопрос: ATZ Ответ: " + dt, dStart);
                                    port.Close();
                                }
                            }
                            continue;
                        }
                        if (data.Length > 4 && data.Substring(1, 4) == "RSSI")
                        {
                            System.Threading.Thread.Sleep(1000);
                            continue;
                        }
                        if (lines[c].Replace("\"", "").Length > 2
                        && lines[c].Replace("\"", "").Substring(0, 2) == "+7")
                        {
                            text = conv(lines[c + 4].Trim());
                            sender = lines[c].Replace("\"", "");
                            Console.WriteLine("\t *** \t *** Новое сообщение ***");
                            Console.WriteLine("\t Дата : \t {0} {1}", lines[c + 2], lines[c + 3].ToString().Substring(0, 8));
                            Console.WriteLine("\t Номер : \t {0}", sender);
                            Console.WriteLine("\t MSG : \t {0}", text);
                            toLog("[MSG] Дата сообщения: " + lines[c + 2] + " " + lines[c + 3].ToString().Substring(0, 8), dStart);
                            toLog("[MSG] Номер: " + sender, dStart);
                            toLog("[MSG] " + text, dStart);
                            if (cfgPhones.Contains(sender) || cfgPhones.Length == 0)
                            {
                                Console.WriteLine("\t\t *** Новое сообщение ***");
                                toLog("[MSG][MASTER] получено сообщение от правильного адресата", dStart);
                                try
                                {
                                    Process p = new Process();
                                    p.StartInfo.UseShellExecute = false;
                                    p.StartInfo.RedirectStandardOutput = true;
                                    p.StartInfo.FileName = cmd[text];
                                    p.Start();
                                    outCmd = p.StandardOutput.ReadToEnd();
                                    p.WaitForExit();
                                    // SMS
                                    // send answer
                                    System.Threading.Thread.Sleep(500);
                                    port.WriteLine("AT+CMGS=" + "\"" + sender.Replace("+7", "8").ToString() + "\"" + Environment.NewLine);
                                    port.WriteLine("OK!");
                                    port.WriteLine((char)26 + "");
                                    // SMS end
                                }
                                catch (Exception ex)
                                {
                                    outCmd = ex.Message;
                                }
                                toLog("[CMD] " + outCmd, dStart);
                            }
                            port.DiscardInBuffer();
                            port.DiscardOutBuffer();
                            messg = 0;
                            dt = "";
                            System.Threading.Thread.Sleep(1000);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Err: " + ex.Message + ex.StackTrace);
                    toLog("[ERROR] " + ex.Message, dStart);
                    System.Threading.Thread.Sleep(3000);
                }
            }
        }
        public static string conv(string str)
        {
            char ch;
            string text = "";
            for (int i = 0; i < str.Length; i += 4)
            {
                try
                {
                    Convert.ToInt32(str.Substring(i, 4), 16);
                }
                catch (Exception ex)
                {
                    return str;
                }
                ch = Convert.ToChar(Convert.ToInt32(str.Substring(i, 4), 16));
                text += ch;
            }
            return text;
        }
        public static void toLog(string msg, string dStart)
        {
            StreamWriter log;
            string data = DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss");
            if (!File.Exists("log\\" + dStart.Replace(":", " ") + ".log"))
            {
                log = new StreamWriter("log\\" + dStart.Replace(":", " ") + ".log");
            }
            else
            {
                log = File.AppendText("log\\" + dStart.Replace(":", " ") + ".log");
            }
            log.WriteLine("[" + data + "]" + msg);
            log.Close();
        }
    }
}